import 'dart:io';

void main() {
  print("------------------------------------------------");
  studiKasus1();
  print("------------------------------------------------");
  studiKasus2();
  print("------------------------------------------------");
  studiKasus3();
}

void studiKasus1(){
stdout.write("Apakah anda ingin menginstall aplikasi inin (Y/N)? :" );
var input = stdin.readLineSync();
input == "Y"
      ? print("Anda akan menginstall aplikasi")
      : input == "T"
          ? print("Aborted")
          : print("harus memilih salah satu");
}

void studiKasus2(){
  stdout.write("Masukkan nama anda :" );
  var nama = stdin.readLineSync();

  if (nama == "") {
    print("Nama harus diisi");
  } else {
    print(
        "Pilih Peran Anda, ketikkan angka? \n1. Penyihir \n2. Guard \n3. Werewolf");
    var peran = stdin.readLineSync();

    if (peran == "") {
      print("Hallo, $nama Pilih peranmu untuk memulai game!");
    } else if (peran == "1") {
      print(
          "Selamat datang di Dunia Werewolf, $nama! \nHalo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "2") {
      print(
          "Selamat datang di Dunia Werewolf, $nama! \nHalo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if (peran == "3") {
      print("Selamat datang di Dunia Werewolf, $nama"
          "\nHalo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Tidak ada yang dipilih");
    }
  }
}

void studiKasus3(){
  for (int i = 1; i < 21; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
      print("I Love Coding");
    } else if (i % 2 == 0) {
      print("Berkualitas");
    } else if (i % 2 == 1) {
      print("Santai");
    }
  }
}